<?php


if (!function_exists ('GUID')){
    function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }
    
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}

if (!function_exists ('CheckAuth')){
    function CheckAuth(){        
        if (!isset($_COOKIE['SESSIONID'])){
            header('Location: /login.html');
            return false;
        }
        return true;
    }
}

?>
