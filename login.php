<?php 
require('models.php');
require('database.php');

$response = new Response();

$errors = [];
$username =  $_POST["username"];
$password = $_POST["password"];

if (empty($username)){
    $errors["username"] = "field required";
}
if (empty($password)){
    $errors["password"] = "field required";
}

if(count($errors) == 0){
    $dbresult = login($username, $password);
    echo $dbresult->error;
    if ($dbresult->error == true) {
        $errors["general"] = "server failure";
    }else{
        if(!empty($dbresult->result['uuid'])){
            setcookie('SESSIONID', $dbresult->result['uuid'], $dbresult->result['timeout'], "/");
        }else{
            $errors["username"] = "no such user";
        }
    }
}

if(count($errors) > 0){
    $response->error = true;
    $response->data = $errors;
}

header('Content-type: application/json');
echo json_encode($response);

?>