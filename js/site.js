$.fn.fillErrorMessages = function (messages) {
    $.each(messages, (key, value) => {
        var selector = '[data-validation-msg-for=' + key + ']';
        $(this).find(selector).html(value);
    });
}



$(() => {
    $("[data-submit='form']").click(function () {
        var form = $(this).closest('form');
        var data = form.serialize();
        var url = form.attr('action');

        $(form).find('[data-validation-msg-for]').html('');

        $.post({
            url: url,
            data: data
        }).then((resp) => {
            if (resp.error) {
                form.fillErrorMessages(resp.data);
            } else {
                window.location = $(this).data('success-url');
            }
        });
    });
});