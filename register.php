<?php 
require('models.php');
require('database.php');


$response = new Response();

$errors = [];
$username =  $_POST["username"];
$password = $_POST["password"];
$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];

if (empty($username)){
    $errors["username"] = "field required";
}
if (empty($password)){
    $errors["password"] = "field required";
}

if (empty($firstname)){
    $errors["firstname"] = "field required";
}
if (empty($lastname)){
    $errors["lastname"] = "field required";
}


if(count($errors) == 0){
    $dbresult = register($username, $password, $firstname, $lastname);

    if ($dbresult->error == true) {
        $errors["general"] = "server failure";
    }else{
        if($dbresult->result == false){
            $errors["username"] = "user already exists";
        }
    }
}


if(count($errors) > 0){
    $response->error = true;
    $response->data = $errors;
}

header('Content-type: application/json');
echo json_encode($response);

?>